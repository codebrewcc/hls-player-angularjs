﻿
var gutil = require('gulp-util');

var gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
        pattern: ['gulp-add-src', 'del', 'merge-stream', 'require-dir', 'gulp-src', 'gulp-angular-templatecache']
    }),

    sources = {
        loadTemplates: [
            "src/app/modules/home/**/*.html",
            "src/app/modules/shared/tpls/**/*.html",
        ],
        appTemplates: [
            "!src/app/modules/home/**/*.html",
            "!src/app/modules/shared/tpls/**/*.html",
            "src/app/modules/**/*.html"
        ]
    }

gulp.task('app:templates', function() {
    var loadTemplates = gulp.src(sources.loadTemplates)
        .pipe($.angularTemplatecache('templates_6c6f6164.js', {
            module: 'loadTpls',
            standalone: true,
            base: 'src/',
            transformUrl: function(url) {
                var u = url;
                var i = u.indexOf("app");
                return u.substring(i);
            },
        }))
        .pipe(gulp.dest('./dist/'));

    var appTemplates = gulp.src(sources.appTemplates)
        .pipe($.angularTemplatecache('templates_617070.js', {
            module: 'appTpls',
            standalone: true,
            base: 'src/',
            transformUrl: function(url) {
                var u = url;
                var i = u.indexOf("app");
                return u.substring(i);
            },
        }))
        .pipe(gulp.dest('./dist/'));

    return $.mergeStream(loadTemplates, appTemplates);

});
