﻿
var gutil = require('gulp-util');
var gulp = require('gulp');

var $ = require('gulp-load-plugins')({
        pattern: ['gulp-src', 'gulp-autoprefixer', 'gulp-concat', 'merge-stream', 'gulp-uglify', 'gulp-minify', 'gulp-if', 'gulp-foreach', 'gulp-debug', 'del', 'gulp-sourcemaps', 'gulp-add-src', 'gulp-if', 'gulp-ng-annotate']
    }),
    sources = {
        //the libraries below are now alternatively being loaded via cdns
        coreVendor: [
            // "./bower_components/angular-translate/angular-translate.min.js",
            // "./bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js",
            // "./bower_components/angular-sanitize/angular-sanitize.min.js",
            // "./bower_components/oclazyload/dist/ocLazyLoad.min.js",
        ],

        //this vendor file is loaded when a user is authenticated and the app is loaded, the rest (core libraries i.e. angular js) are loaded via cdn js
        appVendor: [
            "./node_modules/angular-vs-repeat/src/angular-vs-repeat.min.js",
        ],
        appLoad: [
            "src/app/index.module.js",
            "src/app/**/*.module.js",
            "src/app/index.*.js",
            "!src/app/index.*.*.js",
            "src/app/modules/home/home.controller.js"
        ],
        appCore: [
            "src/app/modules/shared/*/shared.*.js",
            "src/app/modules/shared/**/*.*.js",
            "src/app/modules/**/*.*.js",
            "!src/app/modules/home/*.*.js"
        ],
        moduleSets: []
    };

gulp.task('watch:core', function() {
    gulp.watch(sources.coreapp, ['app:core']);
});

gulp.task('app:vendor', function() {
    return gulp.src(sources.appVendor)
        //.pipe($.uglify())
        .pipe($.concat('vendor_636f7265.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('app:load', function() {
    return gulp.src(sources.appLoad)
        .pipe($.if(global.config.isLocal && global.config.sourceMapsEnabled, $.sourcemaps.init()))
        .pipe($.ngAnnotate())
        .pipe($.if(global.config.isLocal && global.config.sourceMapsEnabled, $.sourcemaps.write()))
        .pipe($.concat('app_6c6f6164.js'))
        .pipe($.if(!global.config.isLocal, $.uglify()))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('app:core', function() {
    return gulp.src(sources.appCore)
        .pipe($.if(global.config.isLocal, $.addSrc.append('./src/app/index.api.local.js')))
        .pipe($.if((!global.config.isLocal && (global.config.deployType == "Staging")), $.addSrc.append('./src/app/index.api.staging.js')))
        .pipe($.if((!global.config.isLocal && (global.config.deployType == "PreProduction")), $.addSrc.append('./src/app/index.api.preproduction.js')))
        //.pipe($.jshint())
        //.pipe($.debug())
        //.pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe($.if(global.config.isLocal && global.config.sourceMapsEnabled, $.sourcemaps.init()))
        //.pipe($.if(!global.config.isLocal, $.uglify()))
        .pipe($.ngAnnotate())
        .pipe($.if(global.config.isLocal && global.config.sourceMapsEnabled, $.sourcemaps.write()))
        //.pipe($.if(!global.config.isLocal, $.uglify()))
        .pipe($.concat('app_636f7265.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('app:compile', ['app:load', 'app:core']);
