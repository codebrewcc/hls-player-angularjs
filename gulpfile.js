/// <binding Clean='clean' />
var gutil = require('gulp-util');
var requireDir = require('require-dir');
requireDir('./build.scripts');

global.config = {
    isLocal: true,
    sourceMapsEnabled: false,
    deployType: ""
};

var gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
        pattern: ['gulp-*', 'gulp-add-src', 'del', 'merge-stream', 'require-dir']
    })

gulp.task('html-inject', function() {
    return gulp.src('./src/index.html').pipe(gulp.dest('./dist'));
});

gulp.task('_clean', function() {
    // $.del.sync('./dist');
});

gulp.task('_compile:js', ['app:compile']);
gulp.task('_compile:templates', ['app:templates'])
gulp.task('_compile:css', ['sass:compile']);
gulp.task('_compile:assets', ['other-assets', 'html-inject']);
// Compiles the whole application
gulp.task('_compile:app', ['_clean'], function() {
    gulp.start(['_compile:js', 'app:vendor', '_compile:css', '_compile:templates', '_compile:assets']);
});

gulp.task('_prompt:build', function() {
    return gulp.src('*').pipe(
        $.prompt.prompt({
            type: 'list',
            name: 'method',
            message: 'CRSv4 Task Runner',
            choices: ['Build Local', 'Deploy App']
        }, function(res) {
            switch (res.method) {
                case "Deploy App":
                    global.config.isLocal = false;
                    gulp.start(['_prompt:deployinit']);
                    break;
                case "Build Local":
                    global.config.isLocal = true;
                    gulp.start('_compile:all');
                    break;
            };
        }));
});

gulp.task('_prompt:deployinit', function() {
    return gulp.src('*').pipe(
        $.prompt.prompt({
            type: 'list',
            name: 'method',
            message: 'Choose Deployment Type',
            choices: ['Staging', 'PreProduction', 'Live']
        }, function(res) {
            switch (res.method) {
                case "Staging":
                    global.config.deployType = res.method;
                    gulp.start('_prompt:deploybuild');
                    break;
                case "PreProduction":
                    global.config.deployType = res.method;
                    gulp.start('_prompt:deploybuild');
                    break;
                case "Live":
                    {
                        gutil.log('Live Deployment Currently Unavailable!');
                        gulp.start('_prompt:deployinit');
                    }
            };
        }));
});

gulp.task('default', ['_compile:app']);
// gulp.task('default', ['_prompt:build']);

gulp.task('serve', function() {
    $.connect.server({
        root: './dist',
        port: 8888,
        fallback: 'index.html'
    });
});
