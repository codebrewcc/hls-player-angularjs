(function() {
    'use strict';

    angular
        .module('minima.shared.directives', [])
        .run(function() {
            //run block for directives
        }).

    directive('exampleDirective', function() {
        return {
            restrict: 'E',
            template: '<h1>This is a Directive!</h1>',
            link: function(scope, el) {

            }
        }
    });
})();

(function () {
    'use strict';

    angular
        .module('minima.shared.factories', [])

})();

(function() {
    'use strict';

    angular
        .module('minima.shared.services', [])
  
})();

(function() {
    'use strict';

    AppController.$inject = ["$rootScope", "$scope"];
    angular
        .module('minima')
        .controller('AppController', AppController);

    function AppController($rootScope, $scope) {
        $rootScope.appConfigs = {
          "searchOpen": false
        };

        $scope.unavailableNotification = function(){
          alert('This is not available at the moment! (This is just for show)');
        };

    };
})();


(function() {
    'use strict';

    angular
        .module('minima.shared.factories')
        .factory('enums', enums);

    /** @ngInject */

    function enums() {
        var service = {};

        return service;
    };

})();

(function() {
    'use strict';

    helpersService.$inject = ["lodash"];
    angular
        .module('minima.shared.services')
        .factory('helpersService', helpersService);

    function helpersService(lodash) {
        var service = {};

        service.validateUrl = function(value) {
            return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
        };

        service.prettyPrintTime = function(time) {
            var minutes = Math.floor(time / 60);
            var seconds = time - minutes * 60;

            function str_pad_left(string, pad, length) {
                return (new Array(length + 1).join(pad) + string).slice(-length);
            }

            var finalTime = str_pad_left(minutes, '0', 2) + ':' + str_pad_left(seconds, '0', 2);

            return finalTime
        }

        service.extractAvailableQualities = function(res) {
            var availableQualities = [];

            availableQualities.push({
                "level": -1,
                "name": "auto"
            });

            var level = 0;
            lodash.forEach(res.levels, function(o, k) {
                if (!lodash.includes(lodash.map(availableQualities, 'name'), o.name)) {
                    availableQualities.push({
                        "level": -1,
                        "name": o.name
                    });
                }
            })

            availableQualities = lodash.sortBy(availableQualities, [function(o) {
                return parseInt(o.name);
            }]);
            lodash.forEach(availableQualities, function(o, k) {
                if (o.name !== "auto") {
                    availableQualities[k].level = level;
                    level++;
                }
            });

            return availableQualities;
        }

        return service;
    };
})();

(function() {
    'use strict';

    angular
        .module('minima.shared.services')
        .factory('HlsService', HlsService);

    function HlsService() {
        var service = {};

        service.config = {
            autoStartLoad: true,
            startPosition: -1,
            capLevelToPlayerSize: true,
            debug: false,
            defaultAudioCodec: undefined,
            initialLiveManifestSize: 1,
            maxBufferLength: 30,
            maxMaxBufferLength: 600,
            maxBufferSize: 60 * 1000 * 1000,
            maxBufferHole: 0.5,
            maxSeekHole: 2,
            lowBufferWatchdogPeriod: 0.5,
            highBufferWatchdogPeriod: 3,
            nudgeOffset: 0.1,
            nudgeMaxRetry: 3,
            maxFragLookUpTolerance: 0.2,
            liveSyncDurationCount: 3,
            liveMaxLatencyDurationCount: 10,
            enableWorker: true,
            enableSoftwareAES: true,
            manifestLoadingTimeOut: 10000,
            manifestLoadingMaxRetry: 1,
            manifestLoadingRetryDelay: 500,
            manifestLoadingMaxRetryTimeout: 64000,
            startLevel: undefined,
            levelLoadingTimeOut: 10000,
            levelLoadingMaxRetry: 4,
            levelLoadingRetryDelay: 500,
            levelLoadingMaxRetryTimeout: 64000,
            fragLoadingTimeOut: 20000,
            fragLoadingMaxRetry: 6,
            fragLoadingRetryDelay: 500,
            fragLoadingMaxRetryTimeout: 64000,
            startFragPrefetch: false,
            appendErrorMaxRetry: 3,
            // loader: customLoader,
            // fLoader: customFragmentLoader,
            // pLoader: customPlaylistLoader,
            // xhrSetup: XMLHttpRequestSetupCallback,
            // fetchSetup: FetchSetupCallback,
            // abrController: customAbrController,
            // timelineController: TimelineController,
            enableCEA708Captions: true,
            stretchShortVideoTrack: false,
            forceKeyFrameOnDiscontinuity: true,
            abrEwmaFastLive: 5.0,
            abrEwmaSlowLive: 9.0,
            abrEwmaFastVoD: 4.0,
            abrEwmaSlowVoD: 15.0,
            abrEwmaDefaultEstimate: 500000,
            abrBandWidthFactor: 0.8,
            abrBandWidthUpFactor: 0.7,
            minAutoBitrate: 0
        }
        service.Manager = new Hls(service.config);

        service.reInitializeManager = function() {
            service.Manager = {};
            service.Manager = new Hls(service.config);
        };

        return service;
    };
})();


(function() {
    'use strict';

    informationService.$inject = ["$http", "$q", "apiService"];
    angular
        .module('minima.shared.services')
        .factory('informationService', informationService);

    function informationService($http, $q, apiService) {
        var service = {};

        service.getVideos = function() {
            var deferred = $q.defer();
            var res = [{
                    "id": "",
                    "url": "http://www.streambox.fr/playlists/x36xhzz/x36xhzz.m3u8",
                    "name": "Big Buck Bunny"
                },
                // {
                //     "id": "",
                //     "url": "http://clips.vorwaerts-gmbh.de/VfE.webm",
                //     "name": "Sample Video"
                // }
            ];

            deferred.resolve(res);

            return deferred.promise;
        };

        service.getVideoDetailsByUrl = function() {

        };

        return service;
    };
})();

(function() {
    'use strict';

    searchBox.$inject = ["informationService", "helpersService", "$state"];
    angular
        .module('minima.shared.directives')
        .directive('searchBox', searchBox);

    /** @ngInject */
    function searchBox(informationService, helpersService, $state) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/modules/shared/directives/search-box/search-box.html',
            scope: {
                "isOpen": "="
            },
            controller: ["$scope", "$timeout", function($scope, $timeout) {

                var vm = this;

                $scope.dom = {
                    "inputSearch": document.getElementById('search__input'),
                    "searchModel": "",
                    "playVideo": function(url) {
                        if (helpersService.validateUrl(url)) {
                            $state.go('app.base.player', {
                                "url": url
                            });
                            $scope.isOpen = false;
                        } else {
                            alert('invalid url');
                        };
                    },
                    "videosList": []
                }

                function initEvents() {
                    document.addEventListener('keyup', function(ev) {
                        // escape key.
                        if (ev.keyCode == 27) {
                            $scope.isOpen = false;
                            $scope.$apply();

                        }
                    });
                }

                $scope.$watch('isOpen', function(nv) {
                    if (nv) {
                        $timeout(function() {
                            $scope.dom.inputSearch.focus();
                        }, 500);
                    } else {
                        $scope.dom.inputSearch.blur();
                    }
                });

                vm.init = function() {
                    initEvents();
                    informationService.getVideos().then(function(res) {
                        angular.copy(res, $scope.dom.videosList);
                    });
                }

                vm.init();
            }]
        };
    }
})();

(function () {
    'use strict';

    angular
        .module('minima.shared', [
            'minima.shared.directives',
            'minima.shared.factories',
            'minima.shared.services'
        ])
        .config(config);

    /** @ngInject */
    function config() {

    }
})();

(function() {
    'use strict';

    DockController.$inject = ["$state"];
    angular
        .module('minima')
        .controller('DockController', DockController);

    function DockController($state) {

        var vm = this;

        vm.dom = {
          "playVideo": function(){
            $state.go('app.base.player', {"url": "http://www.streambox.fr/playlists/x36xhzz/x36xhzz.m3u8" });
          }
        }
    };
})();

(function() {
    'use strict';

    PlayerController.$inject = ["$state", "$timeout", "$scope", "HlsService", "helpersService"];
    angular
        .module('minima')
        .controller('PlayerController', PlayerController);

    function PlayerController($state, $timeout, $scope, HlsService, helpersService) {

        var vm = this;

        var videoUrl = $state.params.url;
        // var videoUrl = "http://videoplayer.minimainc/assets/video/beesqueeze_leaves.mp4";

        vm.dom = {
            "playing": false,
            "video": document.getElementById('video'),
            "availableQualities": [],
            "volumeControl": 50,
            "isFullScreen": false,
            "currentLevel": 1,
            "currentTime": 0,
            "showSource": false,
            "videoDetails": {
                "name": "",
                "totalLength": 0
            }
        };

        vm.playerFns = {
            "changeQuality": function(qualityLevel) {
                HlsService.Manager.nextLevel = qualityLevel;
            },
            "evaluateTime": function(time) {
                return helpersService.prettyPrintTime(time);
            },
            "evaluateTimeIndicator": function(time) {

            }
        };

        vm.init = function() {
            if (Hls.isSupported()) {
                HlsService.reInitializeManager();
                HlsService.Manager.loadSource(videoUrl);
                HlsService.Manager.attachMedia(video);
                HlsService.Manager.on(Hls.Events.MANIFEST_PARSED, function(e, res) {
                    console.info(res);
                    $timeout(function() {
                        vm.dom.video.play();
                        vm.dom.playing = true;
                    }, 100);
                });

                HlsService.Manager.on(Hls.Events.ERROR, function(event, data) {
                    var errorType = data.type;
                    var errorDetails = data.details;
                    var errorFatal = data.fatal;

                    alert('An Error Has occured!');

                    // switch (data.details) {
                    //     case HlsService.Manager.ErrorDetails.FRAG_LOAD_ERROR:
                    //         break;
                    //     default:
                    //         break;
                    // }
                });

                HlsService.Manager.on(Hls.Events.MANIFEST_LOADED, function(e, res) {

                    vm.dom.availableQualities.length = 0;

                    angular.copy(helpersService.extractAvailableQualities(res), vm.dom.availableQualities);
                });

                // $scope.$apply();
            }
        }

        $scope.$watch('vm.dom.playing', function(nv) {
            if (nv) {
                vm.dom.video.play();
            } else {
                vm.dom.video.pause();
            }
        });

        //updating currently selected level
        setInterval(function() {
            var level = HlsService.Manager.loadLevel;
            vm.dom.currentLevel = level;
        }, 3500);

        vm.init();

        var initDuration = true;

        // set event listener to execute on timeupdate. This gets invoked every ~250ms or so
        $('#video').on('timeupdate', function() {
            // use parseInt to round to whole seconds
            var ct = parseInt(this.currentTime);
            if (initDuration) {
                initDuration = false;
                vm.dom.videoDetails.totalLength = vm.dom.video.duration;
            }
            this.lastTime = ct;
            vm.dom.currentTime = ct;
            $scope.$apply();
        });

        //simply modifying background size, ui.
        $('input[type=range]').on('input', function(e) {
            var min = e.target.min,
                max = e.target.max,
                val = e.target.value;

            $(e.target).css({
                'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
            });
        }).trigger('input');

        //updating volume
        $scope.$watch('vm.dom.volumeControl', function(nv) {
            vm.dom.video.volume = (nv / 100);
        });

    };
})();

(function () {
    'use strict';

    angular
        .module('minima')
        .factory('apiService', apiService);

    /** @ngInject */
    function apiService() {

        var api = {};

        api.ssoBaseUrl = '';
        api.apiBaseUrl = '';
        api.appBaseUrl = '';

        return api;
    }

})();
