(function() {
    'use strict';

    angular
        .module('minima', [
            'loadTpls',

            // Load Core Libraries
            'ui.router',
            'oc.lazyLoad',
            'ngLodash'

            //Load App Modules
            //ex. minima.home
        ]);
})();

(function () {
    'use strict';

    angular
        .module('minima.shared', [
            'minima.shared.directives',
            'minima.shared.factories',
            'minima.shared.services'
        ])
        .config(config);

    /** @ngInject */
    function config() {

    }
})();

(function () {
    'use strict';

    config.$inject = ["$httpProvider", "$compileProvider"];
    angular
        .module('minima')
        .config(config);

    /** @ngInject */
    function config($httpProvider, $compileProvider) {

        //to use debugger if published call this method in the console: angular.reloadWithDebugInfo();
        $compileProvider.debugInfoEnabled(false);
    }

})();

(function () {
    'use strict';

    assetsProvider.$inject = ["$injector"];
    angular
        .module('minima.constants', []
        )
        .provider('assets', assetsProvider);

    /** @ngInject */
    function assetsProvider($injector) {

        var assetsConstants = {

        };

        //return new Assets(assetsConstants);
    }
})();

(function () {
    'use strict';

    IndexController.$inject = ["$scope"];
    angular
        .module('minima')
        .controller('IndexController', IndexController);

    /** @ngInject */
    function IndexController($scope) {
        var vm = this;


    };
})();

(function () {
    'use strict';

    routeConfig.$inject = ["$stateProvider", "$urlRouterProvider", "$locationProvider", "$ocLazyLoadProvider"];
    angular
        .module('minima')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $ocLazyLoadProvider) {

        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/');

        $stateProvider.
            // Main Layout Structure
            state('app', {
                abstract: true,
                template: '<ui-view></ui-view>',
                url: '',
            })
            .state('app.home', {
                url: '/home',
                templateUrl: 'app/modules/home/home.html',
                controller: 'HomeController as vm'
            })
            .state('app.base', {
                abstract: true,
                url: '',
                templateUrl: 'app/modules/shared/tpls/app.html',
                controller: 'AppController as vm',
                resolve: {
                    "minima.shared": ["$ocLazyLoad", function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            [
                                'app_636f7265.js', //loading js for app
                                'vendor_636f7265.js', //loading libraries for app
                                'templates_617070.js', //loading templates for app
                            ]
                        ]);
                    }],
                    "hls": ["$ocLazyLoad", function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            [
                                '//cdn.jsdelivr.net/hls.js/latest/hls.min.js', //Video library
                            ]
                        ]);
                    }]
                }
            })
            .state('app.base.dock', {
                url: '/',
                templateUrl: 'app/modules/dock/dock.html',
                controller: 'DockController as vm',
            })
            .state('app.base.player', {
                url: '/watch?url',
                templateUrl: 'app/modules/player/player.html',
                controller: 'PlayerController as vm',
            });
    }
})();


(function() {
    'use strict';

    angular
        .module('minima')
        .run(runBlock);

    function runBlock() {
      
    }
})();

(function() {
    'use strict';

    HomeController.$inject = ["$state", "$timeout"];
    angular
        .module('minima')
        .controller('HomeController', HomeController);

    function HomeController($state, $timeout) {

        var vm = this;

        vm.init = function() {
            $timeout(function() {
                $state.go('app.base.dock')
            });
        }

        vm.init();

    };
})();
