# Minima Video. Player

This is an app exhibiting html5 video usage and utilizing the HLS library, found [here](http://swapi.co/).

## Requirements
For development, you will only need Node.js as well as Yarn installed on your environment.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [Yarn](https://yarnpkg.com/en/docs/install).
You should be able to run the following commands after the installation procedure
below.

    $ node --version
    v6.10.1

    $ yarn --version
    v0.23.2

## Installation

* $ git clone https://iestyn02@bitbucket.org/minimain/videoplayer.minimainc.git
* $ cd videoplayer.minimainc
* $ yarn install

## Build & Serve

* $ gulp _compile:app
* $ gulp serve
* $ Go to http://localhost:8888 on your browser

## License

MIT
