(function () {
    'use strict';

    angular
        .module('minima')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $ocLazyLoadProvider) {

        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/');

        $stateProvider.
            // Main Layout Structure
            state('app', {
                abstract: true,
                template: '<ui-view></ui-view>',
                url: '',
            })
            .state('app.home', {
                url: '/home',
                templateUrl: 'app/modules/home/home.html',
                controller: 'HomeController as vm'
            })
            .state('app.base', {
                abstract: true,
                url: '',
                templateUrl: 'app/modules/shared/tpls/app.html',
                controller: 'AppController as vm',
                resolve: {
                    "minima.shared": function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            [
                                'app_636f7265.js', //loading js for app
                                'vendor_636f7265.js', //loading libraries for app
                                'templates_617070.js', //loading templates for app
                            ]
                        ]);
                    },
                    "hls": function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            [
                                '//cdn.jsdelivr.net/hls.js/latest/hls.min.js', //Video library
                            ]
                        ]);
                    }
                }
            })
            .state('app.base.dock', {
                url: '/',
                templateUrl: 'app/modules/dock/dock.html',
                controller: 'DockController as vm',
            })
            .state('app.base.player', {
                url: '/watch?url',
                templateUrl: 'app/modules/player/player.html',
                controller: 'PlayerController as vm',
            });
    }
})();
