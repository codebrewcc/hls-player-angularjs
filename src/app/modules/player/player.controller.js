(function() {
    'use strict';

    angular
        .module('minima')
        .controller('PlayerController', PlayerController);

    function PlayerController($state, $timeout, $scope, HlsService, helpersService) {

        var vm = this;

        var videoUrl = $state.params.url;
        // var videoUrl = "http://videoplayer.minimainc/assets/video/beesqueeze_leaves.mp4";

        vm.dom = {
            "playing": false,
            "video": document.getElementById('video'),
            "availableQualities": [],
            "volumeControl": 50,
            "isFullScreen": false,
            "currentLevel": 1,
            "currentTime": 0,
            "showSource": false,
            "videoDetails": {
                "name": "",
                "totalLength": 0
            }
        };

        vm.playerFns = {
            "changeQuality": function(qualityLevel) {
                HlsService.Manager.nextLevel = qualityLevel;
            },
            "evaluateTime": function(time) {
                return helpersService.prettyPrintTime(time);
            },
            "evaluateTimeIndicator": function(time) {

            }
        };

        vm.init = function() {
            if (Hls.isSupported()) {
                HlsService.reInitializeManager();
                HlsService.Manager.loadSource(videoUrl);
                HlsService.Manager.attachMedia(video);
                HlsService.Manager.on(Hls.Events.MANIFEST_PARSED, function(e, res) {
                    $timeout(function() {
                        vm.dom.video.play();
                        vm.dom.playing = true;
                    }, 100);
                });

                HlsService.Manager.on(Hls.Events.ERROR, function(event, data) {
                    var errorType = data.type;
                    var errorDetails = data.details;
                    var errorFatal = data.fatal;

                    alert('An Error Has occured!');

                    // switch (data.details) {
                    //     case HlsService.Manager.ErrorDetails.FRAG_LOAD_ERROR:
                    //         break;
                    //     default:
                    //         break;
                    // }
                });

                HlsService.Manager.on(Hls.Events.MANIFEST_LOADED, function(e, res) {

                    vm.dom.availableQualities.length = 0;

                    angular.copy(helpersService.extractAvailableQualities(res), vm.dom.availableQualities);
                });

                // $scope.$apply();
            }
        }

        $scope.$watch('vm.dom.playing', function(nv) {
            if (nv) {
                vm.dom.video.play();
            } else {
                vm.dom.video.pause();
            }
        });

        //updating currently selected level
        setInterval(function() {
            var level = HlsService.Manager.loadLevel;
            vm.dom.currentLevel = level;
        }, 3500);

        vm.init();

        var initDuration = true;

        // set event listener to execute on timeupdate. This gets invoked every ~250ms or so
        $('#video').on('timeupdate', function() {
            // use parseInt to round to whole seconds
            var ct = parseInt(this.currentTime);
            if (initDuration) {
                initDuration = false;
                vm.dom.videoDetails.totalLength = vm.dom.video.duration;
            }
            this.lastTime = ct;
            vm.dom.currentTime = ct;
            $scope.$apply();
        });

        //simply modifying background size, ui.
        $('input[type=range]').on('input', function(e) {
            var min = e.target.min,
                max = e.target.max,
                val = e.target.value;

            $(e.target).css({
                'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
            });
        }).trigger('input');

        //updating volume
        $scope.$watch('vm.dom.volumeControl', function(nv) {
            vm.dom.video.volume = (nv / 100);
        });

    };
})();
