(function() {
    'use strict';

    angular
        .module('minima')
        .controller('HomeController', HomeController);

    function HomeController($state, $timeout) {

        var vm = this;

        vm.init = function() {
            $timeout(function() {
                $state.go('app.base.dock')
            });
        }

        vm.init();

    };
})();
