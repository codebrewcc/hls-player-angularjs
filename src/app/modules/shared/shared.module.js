﻿(function () {
    'use strict';

    angular
        .module('minima.shared', [
            'minima.shared.directives',
            'minima.shared.factories',
            'minima.shared.services'
        ])
        .config(config);

    /** @ngInject */
    function config() {

    }
})();
