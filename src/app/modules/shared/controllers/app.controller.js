(function() {
    'use strict';

    angular
        .module('minima')
        .controller('AppController', AppController);

    function AppController($rootScope, $scope) {
        $rootScope.appConfigs = {
          "searchOpen": false
        };

        $scope.unavailableNotification = function(){
          alert('This is not available at the moment! (This is just for show)');
        };

    };
})();
