(function() {
    'use strict';

    angular
        .module('minima.shared.services')
        .factory('helpersService', helpersService);

    function helpersService(lodash) {
        var service = {};

        service.validateUrl = function(value) {
            return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
        };

        service.prettyPrintTime = function(time) {
            var minutes = Math.floor(time / 60);
            var seconds = time - minutes * 60;

            function str_pad_left(string, pad, length) {
                return (new Array(length + 1).join(pad) + string).slice(-length);
            }

            var finalTime = str_pad_left(minutes, '0', 2) + ':' + str_pad_left(seconds, '0', 2);

            return finalTime
        }

        service.extractAvailableQualities = function(res) {
            var availableQualities = [];

            availableQualities.push({
                "level": -1,
                "name": "auto"
            });

            var level = 0;
            lodash.forEach(res.levels, function(o, k) {
                if (!lodash.includes(lodash.map(availableQualities, 'name'), o.name)) {
                    availableQualities.push({
                        "level": -1,
                        "name": o.name
                    });
                }
            })

            availableQualities = lodash.sortBy(availableQualities, [function(o) {
                return parseInt(o.name);
            }]);
            lodash.forEach(availableQualities, function(o, k) {
                if (o.name !== "auto") {
                    availableQualities[k].level = level;
                    level++;
                }
            });

            return availableQualities;
        }

        return service;
    };
})();
