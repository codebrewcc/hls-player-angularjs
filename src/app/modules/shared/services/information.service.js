﻿
(function() {
    'use strict';

    angular
        .module('minima.shared.services')
        .factory('informationService', informationService);

    function informationService($http, $q, apiService) {
        var service = {};

        service.getVideos = function() {
            var deferred = $q.defer();
            var res = [{
                    "id": "",
                    "url": "http://www.streambox.fr/playlists/x36xhzz/x36xhzz.m3u8",
                    "name": "Big Buck Bunny"
                },
                // {
                //     "id": "",
                //     "url": "http://clips.vorwaerts-gmbh.de/VfE.webm",
                //     "name": "Sample Video"
                // }
            ];

            deferred.resolve(res);

            return deferred.promise;
        };

        service.getVideoDetailsByUrl = function() {

        };

        return service;
    };
})();
