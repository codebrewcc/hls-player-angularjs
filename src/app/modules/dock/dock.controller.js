(function() {
    'use strict';

    angular
        .module('minima')
        .controller('DockController', DockController);

    function DockController($state) {

        var vm = this;

        vm.dom = {
          "playVideo": function(){
            $state.go('app.base.player', {"url": "http://www.streambox.fr/playlists/x36xhzz/x36xhzz.m3u8" });
          }
        }
    };
})();
