﻿(function () {
    'use strict';

    angular
        .module('minima')
        .factory('apiService', apiService);

    /** @ngInject */
    function apiService() {

        var api = {};

        api.ssoBaseUrl = '';
        api.apiBaseUrl = '';
        api.appBaseUrl = '';

        return api;
    }

})();
