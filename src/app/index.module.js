(function() {
    'use strict';

    angular
        .module('minima', [
            'loadTpls',

            // Load Core Libraries
            'ui.router',
            'oc.lazyLoad',
            'ngLodash'

            //Load App Modules
            //ex. minima.home
        ]);
})();
